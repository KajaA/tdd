public class Primenumbers {
	public static void main(String[] args) {

		Primenumbers kaja = new Primenumbers();
		kaja.restore();

	}

	public static boolean isprime(int num) {

		if ((num % 2) == 1) {
			if (num < 8) {
				return !(num == 1);
			} else {
				int factor = 3;
				while ((factor * factor) <= num) {
					if ((num % factor) == 0)
						return false;
					factor += 2;
				}
				return true;
			}
		} else {
			return num == 2;
		}
	}

	/*
	 * Public: check the smallest number divided by all of the numbers from 1 to 20
	 * 
	 * number - the number to be checked
	 * 
	 * Return the smallest searched number
	 */

	public static int restore() {
		final int MAXIMUM = 20;
		int i = 2;
		boolean try_product = true;
		int total = 1;
		while (i <= MAXIMUM) {
			if (isprime(i)) {
				if (try_product) {
					if ((i * i) > MAXIMUM) {
						try_product = false;
					} else {
						int tmp = i;
						int last = tmp;
						while (tmp <= MAXIMUM) {
							last = tmp;
							tmp *= i;
						}
						total *= last;
					}
				}
				if (!(try_product))
					total *= i;

			}
			i++;
		}

		System.out.println(total);
		return (total);
	}
}
